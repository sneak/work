#!/bin/bash

TD="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd)"

cd "$TD"

apt install -y $(cat packages.txt)
