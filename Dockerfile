FROM ubuntu@sha256:8e1c1ee12a539d652c371ee2f4ee66909f4f5fd8002936d8011d958f05faf989

ENV DEBIAN_FRONTEND noninteractive

ARG UBUNTU_MIRROR=http://us.archive.ubuntu.com/ubuntu

ARG UID_TO_ADD=1000
ARG GID_TO_ADD=1000
ARG USERNAME_TO_ADD=sneak

RUN \
	echo "deb $UBUNTU_MIRROR focal main universe restricted multiverse" > /etc/apt/sources.list.new && \
	echo "deb $UBUNTU_MIRROR focal-updates main universe restricted multiverse" >> /etc/apt/sources.list.new && \
	echo "deb $UBUNTU_MIRROR focal-security main universe restricted multiverse" >> /etc/apt/sources.list.new && \
	echo "deb $UBUNTU_MIRROR focal-backports main universe restricted multiverse" >> /etc/apt/sources.list.new && \
	mv /etc/apt/sources.list.new /etc/apt/sources.list

ADD https://sneak.cloud/authorized_keys /root/.ssh/authorized_keys

# we only install essential system packages here
ARG SYSTEM_PACKAGES="build-essential ca-certificates openssh-server git golang rsync runit locales"

RUN \
    apt update && \
    apt -y upgrade && \
    apt -y install $SYSTEM_PACKAGES && \
    groupadd -g $GID_TO_ADD $USERNAME_TO_ADD && \
    useradd -u $UID_TO_ADD -g $GID_TO_ADD -s /bin/bash $USERNAME_TO_ADD && \
    usermod -p '*' $USERNAME_TO_ADD && \
    go get -v -u github.com/peterbourgon/runsvinit && \
    cp /root/go/bin/runsvinit /usr/local/sbin/runsvinit && \
    rm -rf /root/go && \
    rm /etc/ssh/*host*key*

COPY ./setup /tmp/setup
RUN bash /tmp/setup/run.sh

COPY ./root /

RUN chmod +rx /etc/service/*/run

CMD ["/usr/local/sbin/runsvinit"]
